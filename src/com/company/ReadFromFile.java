package com.company;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ReadFromFile {

    public static List<String> readfile2list(String path) {
        List<String> lines1 = null;
        try {
            lines1 = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        } catch (IOException e) {
            try {
                lines1 = Files.readAllLines(Paths.get(path), Charset.forName("CP1251"));
            } catch (IOException e1) {
                System.out.println("ошибка чтения");
                e1.printStackTrace();
            }
        } finally {
            return lines1;
        }
    }

}
